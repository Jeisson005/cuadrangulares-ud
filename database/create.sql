-- public.equipos definition

-- Drop table

-- DROP TABLE equipos;

CREATE TABLE equipos (
	id_equipo serial NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT equipos_pk PRIMARY KEY (id_equipo)
);

-- public.partidos definition

-- Drop table

-- DROP TABLE partidos;

CREATE TABLE partidos (
	id_partido serial NOT NULL,
	id_equipo1 int4 NOT NULL,
	id_equipo2 int4 NOT NULL,
	goles_equipo1 int2 NULL,
	goles_equipo2 int2 NULL,
	CONSTRAINT partidos_pk PRIMARY KEY (id_partido)
);


-- public.partidos foreign keys

ALTER TABLE public.partidos ADD CONSTRAINT partidos_equipos_fk FOREIGN KEY (id_equipo1) REFERENCES equipos(id_equipo) ON DELETE CASCADE;
ALTER TABLE public.partidos ADD CONSTRAINT partidos_equipos_fk2 FOREIGN KEY (id_equipo2) REFERENCES equipos(id_equipo) ON DELETE CASCADE;