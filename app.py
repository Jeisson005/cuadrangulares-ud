#Dependencias
from flask import Flask, render_template, session, redirect, url_for, request, send_from_directory
from flask import render_template
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from urllib.parse import quote
from sqlalchemy.sql import select
from sqlalchemy.orm import aliased
import os


app = Flask(__name__)
DATABASE_URL = os.environ.get('DATABASE_URL')
if DATABASE_URL is None:
    DATABASE_URL = 'postgresql+psycopg2://ykviwlgaznvhuj:6682bbc03f2035abf10a10bcdd4a51269a24d52d5cf1842cb8e9798be0d693cb@ec2-50-19-26-235.compute-1.amazonaws.com:5432/d9rdp2s4bnc9ab'
else:
    DATABASE_URL = DATABASE_URL.replace('postgres','postgresql+psycopg2')
base = automap_base()
engine = create_engine(DATABASE_URL,\
connect_args={'sslmode':'require'})
base.prepare(engine, reflect=True)

@app.route('/equipos')
@app.route('/')
def equipos():
    equiposTable = base.classes.equipos
    db = Session(engine)
    equipos = db.execute(select([equiposTable]))
    equiposList = []
    for equipo in equipos:
        equiposList.append(equipo)
    return render_template('equipos.html', title = 'Equipos', equipos = equiposList)

@app.route('/subir-equipo/<idEquipo>')
def subirEquipo(idEquipo):
    equiposTable = base.classes.equipos
    db = Session(engine)
    if idEquipo == '0':
        equipo = equiposTable(nombre=request.args['nombre'])
        db.add(equipo)
        db.commit()
        return equipos()
    else:
        db.query(equiposTable).filter(equiposTable.id_equipo == idEquipo)\
            .update({equiposTable.nombre:request.args['nombre']}, \
            synchronize_session=False)
        db.commit()
        return equipos()

@app.route('/eliminar-equipo/<idEquipo>')
def eliminarEquipo(idEquipo):
    equiposTable = base.classes.equipos
    db = Session(engine)
    db.query(equiposTable).filter(equiposTable.id_equipo == idEquipo)\
        .delete(synchronize_session=False)
    db.commit()
    return equipos()

@app.route('/partidos')
def partidos():
    equiposTable = base.classes.equipos
    db = Session(engine)
    equipos = db.execute(select([equiposTable]))
    equiposList1 = []
    equiposList2 = []
    for equipo in equipos:
        equiposList1.append(equipo)
        equiposList2.append(equipo)
    equiposCombinaciones = []
    equiposList2.pop(0)
    for equipo in equiposList1:
        for equipo2 in equiposList2:
            equiposCombinaciones.append([equipo,equipo2])
        if equiposList2:
            equiposList2.pop(0)

    partidosTable = base.classes.partidos
    equiposTable1 = aliased(base.classes.equipos)
    equiposTable2 = aliased(base.classes.equipos)
    db = Session(engine)
    partidos = db.query(partidosTable,equiposTable1,equiposTable2)\
        .join(equiposTable1, partidosTable.id_equipo1==equiposTable1.id_equipo)\
        .join(equiposTable2, partidosTable.id_equipo2==equiposTable2.id_equipo)\
    .all()
    partidosList = [{"partido": partido[0].__dict__, "equipo1": partido[1].__dict__, "equipo2": partido[2].__dict__} for partido in partidos]
    for combinacion in equiposCombinaciones:
        existe = False
        eq1 = combinacion[0].id_equipo
        eq2 = combinacion[1].id_equipo
        for partido in partidosList:
            par1 = partido['partido']['id_equipo1']
            par2 = partido['partido']['id_equipo2']
            if (eq1 == par1 and eq2 == par2) or (eq1 == par2 and eq2 == par1):
                existe = True
                break
        if not existe:
            partidosList.append({'partido':{'id_partido':0,'id_equipo1':eq1, 'id_equipo2':eq2},
            'equipo1':{'id_equipo':eq1,'nombre':combinacion[0].nombre},
            'equipo2':{'id_equipo':eq2,'nombre':combinacion[1].nombre}
            })

    return render_template('partidos.html', title = 'Partidos', partidos = partidosList)

@app.route('/subir-partido/<idPartido>')
def subirPartido(idPartido):
    partidosTable = base.classes.partidos
    db = Session(engine)
    if idPartido  == '0':
        partido = partidosTable(goles_equipo1=request.args['goles_equipo1'],
            goles_equipo2=request.args['goles_equipo2'],
            id_equipo1=request.args['id_equipo1'],
            id_equipo2=request.args['id_equipo2'])
        db.add(partido)
        db.commit()
        return partidos()
    else:
        db.query(partidosTable).filter(partidosTable.id_partido == idPartido)\
            .update({partidosTable.goles_equipo1:request.args['goles_equipo1'],
            partidosTable.goles_equipo2:request.args['goles_equipo2']}, \
            synchronize_session=False)
        db.commit()
        return partidos()

@app.route('/eliminar-partido/<idPartido>')
def eliminarPartido(idPartido):
    partidosTable = base.classes.partidos
    db = Session(engine)
    db.query(partidosTable).filter(partidosTable.id_partido == idPartido)\
        .delete(synchronize_session=False)
    db.commit()
    return partidos()

@app.route('/resultados')
def resultados():
    equiposTable = base.classes.equipos
    partidosTable = base.classes.partidos
    db = Session(engine)
    equipos = [eq for eq in db.execute(select([equiposTable]))]
    partidos = [par for par in db.execute(select([partidosTable]))]
    resultadosList = []
    for equipo in equipos:
        equipoDict = {'nombre':equipo.nombre,'pj':0,'pg':0,'pe':0,'pp':0,'gf':0,'gc':0,'PT':0}
        for partido in partidos:
            perdidos = ganados = golesFavor = golesContra = 0
            esEq1 = equipo.id_equipo == partido.id_equipo1
            esEq2 = equipo.id_equipo == partido.id_equipo2
            if esEq1 or esEq2:
                equipoDict['pj']+=1
                if partido.goles_equipo1 > partido.goles_equipo2:
                    ganados+=1
                if partido.goles_equipo1 == partido.goles_equipo2:
                    equipoDict['pe']+=1
                    equipoDict['PT']+=1
                if partido.goles_equipo1 < partido.goles_equipo2:
                    perdidos+=1
                golesFavor+=partido.goles_equipo1
                golesContra+=partido.goles_equipo2
            if esEq1:
                equipoDict['pg'] += ganados
                equipoDict['pp'] += perdidos
                equipoDict['gf'] += golesFavor
                equipoDict['gc'] += golesContra
                equipoDict['PT'] += ganados*3
            if esEq2:
                equipoDict['pg'] += perdidos
                equipoDict['pp'] += ganados
                equipoDict['gf'] += golesContra
                equipoDict['gc'] += golesFavor
                equipoDict['PT'] += perdidos*3
        resultadosList.append(equipoDict)
    return render_template('resultados.html', title = 'Resultados', resultados = resultadosList)