## Sobre el proyecto
Sistema de enfrentamiento todos contra todos.
Enfrenta un número de equipos, todos contra todos.
Cuando son cuatro equipos, se llama Cuadrangular

## Arquitectura
Se cuenta con un framework que permite poner a disposición un conjunto de servicios en Python que hacen uso de la conexión a los datos para su funcionamiento.
La aplicación cuenta con una base de datos que funciona sobre el motor Postgresql, un sistema de plantillas para la visualización en HTML.

## Tecnologías
- Python
- Flask
- Gunicorn
- SQLAlchemy
- Postgresql
- Bootstrap (HTML, CSS, JS)